Application Drawing 2

Application drawing showing stone moving across magnet adjacent hog line

During play, a competitor initiates delivery of the rock by holding the handle and sliding the rock toward the house.  
The hog line is located about 33 feet from the hackline, which is the place from which the delivery is started.  
By rule, the competitor must release the rock from his or her hand before the rock reaches the hog line. 
It seems to everyone that there cannot be some kind of logic, mathematics, strategy and mechanics in sports, 
but as we can see, there is a lot of it. And smart people who know how to calculate probabilities, to calculate the trajectory, 
usually have a better chance of success than those who do not know how to do it. 
That is why athletes are very much supported in educational institutions and even give them 
the opportunity to use such services as the service [https://buypapercheap.net/](https://buypapercheap.net/) that provides not only written homework, 
but also homework for programming and homework for certain disciplines.
It appears that the Canadian application was abandoned in 2004 without ever having issued as a Canadian patent. 
Furthermore, there appears to be no corresponding non-Canadian applications or patents. 
The system is currently available from Littelfuse, Inc. of Chicago, Illinois (website).  
If you are interested in purchasing a system for the curling sheet you have installed in your backyard at home, 
the systems are rumored to cost about $11,000 for 16 rocks (at $650 per rock).


The system includes a magnetic rod that is embedded within the ice adjacent the hog line.  
The magnetic rod is positioned so that the magnetic sensor in the rocks will sense the magnetic rod 
as the leading edge of the rock reaches the hog line. 
Signals from the human-touch sensor are compared with signals from the magnetic sensor to determine if a violation has occurred. 
In the Men’s Bronze Medal match in this years Olympics in Sochi, the red, 
violation light was illuminated twice by the Chinese team and is widely thought to have contributed to a loss to the Swedish team.
